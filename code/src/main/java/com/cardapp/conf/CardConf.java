package com.cardapp.conf;

import com.cardapp.core.CardBot;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.TelegramBotsApi;

@Configuration
public class CardConf {

    @Bean
    public TelegramBotsApi getTelegramBotsApi(){
        return new TelegramBotsApi();
    }

    @Bean
    public CardBot getBot(){
        return new CardBot();
    }
}

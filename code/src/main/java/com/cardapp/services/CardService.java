package com.cardapp.services;

import com.cardapp.models.Card;

import java.util.List;

public interface CardService {
    void createCard(Card card) throws Exception;
    void updateCard(Card card);
    void delCard(Card card);
    List<Card> getCards();
    Card getCard(Long id);
}

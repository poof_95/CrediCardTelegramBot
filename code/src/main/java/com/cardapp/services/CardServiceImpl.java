package com.cardapp.services;

import com.cardapp.models.Card;
import com.cardapp.repository.CardRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private final CardRepo cardRepo;

    public CardServiceImpl(CardRepo cardRepo) {
        this.cardRepo = cardRepo;
    }

    @Override
    public void createCard(Card card) throws Exception {
        cardRepo.save(card);
    }

    @Override
    public void updateCard(Card card) {
        cardRepo.save(card);
    }

    @Override
    public void delCard(Card card) {
        cardRepo.delete(card);
    }

    @Override
    public List<Card> getCards() {
        return (List<Card>) cardRepo.findAll();
    }

    @Override
    public Card getCard(Long id) {
        return cardRepo.findById(id).get();
    }

}

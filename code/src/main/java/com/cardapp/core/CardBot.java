package com.cardapp.core;

import com.cardapp.models.Card;
import com.cardapp.services.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

public class CardBot extends TelegramLongPollingBot {

    @Autowired
    TelegramBotsApi telegramBotsApi;
    @Autowired
    CardService service;

    private static final String HELLO_MSG = "Hello, my lord!";
    private static final String INFO_MSG = "Select card!";
    private static final String SELECT_MSG = "Select other card!";

    private static final String START_CMD = "/start";
    private static final String CECK_CARDS_CMD = "/check_cards";

    @Value("${telegram.username}")
    private String botUsername;
    @Value("${telegram.token}")
    private String botToken;
    @Value("${telegram.user.admin}")
    private String admin;

    private ReplyKeyboardMarkup markup;

    private List<KeyboardRow> buttons;

    private void sendMSG(SendMessage message) {
        try {
            sendMessage(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            SendMessage message = new SendMessage();
            Message msg = update.getMessage();
            message.setChatId(msg.getChatId());
            if (admin.equals(msg.getFrom().getUserName())) {
                String textMSG = msg.getText();
                switch(textMSG){
                    case START_CMD:{
                        message.setText(HELLO_MSG + "\n" + INFO_MSG);
                    }break;
                    case CECK_CARDS_CMD:{
                        List<Card> cards = service.getCards();
                        if (cards.isEmpty())
                            message.setText("No cards!");
                        else
                            message.setText(INFO_MSG);
                    }break;
                    default:{
                        List<Card> cards = service.getCards();
                        if (!cards.isEmpty()) {
                            for (Card card : cards) {
                                if (card.getName().equals(textMSG)) {
                                    message.setText(card + "\n" + INFO_MSG);
                                    break;
                                }else
                                    message.setText("No card!\n" + SELECT_MSG);
                            }
                        }else
                            message.setText("No cards!");
                    }
                }
                message.setReplyMarkup(markup);
            } else {
                message.setText("Access is blocked!");
            }
            sendMSG(message);
        }
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @PostConstruct
    private void init() throws Exception {
        telegramBotsApi.registerBot(this);
        markup = new ReplyKeyboardMarkup();
        buttons = new ArrayList<>();
        new UpdateCardThread(service, markup, buttons);
    }
}

package com.cardapp.core;

import com.cardapp.models.Card;
import com.cardapp.services.CardService;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.List;

public class UpdateCardThread extends Thread {

    private static final String CHECK_BUTTON = "/check_cards";

    private final CardService service;

    private ReplyKeyboardMarkup markup;

    private List<KeyboardRow> buttons;

    public UpdateCardThread(CardService service, ReplyKeyboardMarkup markup, List<KeyboardRow> buttons) {
        this.service = service;
        this.buttons = buttons;
        this.markup = markup;
        start();
    }

    @Override
    public void run() {
        while (true) {
            List<Card> cards = service.getCards();
            if (cards.isEmpty() || buttons.size() != cards.size()
                    || (buttons.size() == cards.size() && buttons.get(0).get(0).getText().equals(CHECK_BUTTON))) {
                buttons.clear();
                if (!cards.isEmpty()) {
                    for (Card card : cards) {
                        KeyboardRow button = new KeyboardRow();
                        button.add(card.getName());
                        buttons.add(button);
                    }
                }else {
                    KeyboardRow button = new KeyboardRow();
                    button.add(CHECK_BUTTON);
                    buttons.add(button);
                }
            }
            markup.setKeyboard(buttons);
            markup.setResizeKeyboard(true);
            markup.setSelective(true);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

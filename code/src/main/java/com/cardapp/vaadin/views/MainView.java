package com.cardapp.vaadin.views;

import com.cardapp.models.Card;
import com.cardapp.services.CardService;
import com.cardapp.vaadin.forms.CardEditForm;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import java.util.Set;

@Route(value = "")
public class MainView extends VerticalLayout {

    private final CardEditForm editForm;
    private final CardService cardService;

    private final Button addCard;
    private final Button dellCard;
    private final Button editCard;
    private final HorizontalLayout controls;
    private final HorizontalLayout content;
    private final Grid<Card> cardGrid;

    public MainView(CardEditForm editForm, CardService cardService) {
        this.editForm = editForm;
        this.cardService = cardService;
        this.addCard = new Button("Add card", VaadinIcon.PLUS_CIRCLE.create());
        this.dellCard = new Button("Delete card", VaadinIcon.TRASH.create());
        this.editCard = new Button("Edit card", VaadinIcon.EDIT.create());
        this.cardGrid = new Grid<>();
        this.controls = new HorizontalLayout(addCard, editCard, dellCard);
        this.content = new HorizontalLayout(cardGrid, editForm);

        content.setWidth("100%");
        content.setHeight("530px");

        editForm.setWidth("100%");
        editForm.setHeight("100%");
        editForm.setVisible(false);
        editForm.setMainView(this);

        cardGrid.setWidth("100%");
        cardGrid.setHeight("100%");

        dellCard.setEnabled(false);
        editCard.setEnabled(false);

        cardGrid.addColumn(Card::getName).setHeader("Name");
        cardGrid.addColumn(Card::getNumber).setHeader("Number");
        cardGrid.addColumn(Card::getDate).setHeader("Date");

        cardGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        cardGrid.asMultiSelect().addValueChangeListener(e -> {
            if (e.getValue() != null) {
                if (e.getValue().size() == 1) {
                    dellCard.setEnabled(true);
                    editCard.setEnabled(true);
                } else if (e.getValue().size() > 1) {
                    dellCard.setEnabled(true);
                    editCard.setEnabled(false);
                } else {
                    dellCard.setEnabled(false);
                    editCard.setEnabled(false);
                    editForm.setVisible(false);
                }
            }
        });

        addCard.addClickListener(e -> editForm.setCard(new Card()));
        editCard.addClickListener(e -> editForm.setCard(cardGrid.getSelectedItems().iterator().next()));
        dellCard.addClickListener(e -> {
            if (cardGrid.getSelectedItems().size() == 1) {
                Card dellCandidate = cardGrid.getSelectedItems().iterator().next();
                cardService.delCard(dellCandidate);
                editForm.setVisible(false);
            } else {
                Set<Card> dellCandidates = cardGrid.getSelectedItems();
                for (Card dellCandidate : dellCandidates)
                    cardService.delCard(dellCandidate);
            }
            updateCardList();
            dellCard.setEnabled(false);
        });

        add(controls, content);
        setMargin(false);

        updateCardList();
    }

    public void updateCardList() {
        cardGrid.setItems(cardService.getCards());
    }

}

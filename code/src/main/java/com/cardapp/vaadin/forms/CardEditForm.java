package com.cardapp.vaadin.forms;

import com.cardapp.models.Card;
import com.cardapp.services.CardService;
import com.cardapp.vaadin.views.MainView;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.regex.Pattern;

@SpringComponent
@UIScope
public class CardEditForm extends FormLayout {

    private final CardService cardService;

    private MainView mv;
    private Card card;

    private String namePattern;
    private String numberPattern;
    private String datePattern;

    private Binder<Card> binder;
    private TextField name;
    private TextField number;
    private TextField date;
    private Button save;
    private Button cancel;
    private HorizontalLayout buttons;

    @Autowired
    public CardEditForm(CardService cardService) {
        this.cardService = cardService;

        this.namePattern = "^[\\w\\s*]{3,}$";
        this.numberPattern = "^[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}\\s[\\d]{4}$";
        this.datePattern = "^[\\d]{2}/[\\d]{2}$";

        binder = new Binder<>(Card.class);
        name = new TextField("Name:");
        number = new TextField("Number:");
        date = new TextField("Date:");
        save = new Button("Save", VaadinIcon.CHECK.create());
        cancel = new Button("Cancel", VaadinIcon.BACKSPACE.create());
        buttons = new HorizontalLayout(save, cancel);

        name.setWidth("100%");
        number.setWidth("100%");
        date.setWidth("100%");
        save.setWidth("100%");
        cancel.setWidth("100%");
        buttons.setWidth("100%");

        name.setPlaceholder("Enter name card");
        number.setPlaceholder("XXXX XXXX XXXX XXXX");
        date.setPlaceholder("XX/XX");

        prepareFields();

        save.addClickListener(e -> save());
        cancel.addClickListener(e -> exit());

        add(name, number, date, buttons);
    }

    public void setMainView(MainView mv){
        if (mv != null)
            this.mv = mv;
    }

    public void setCard(Card card) {
        this.card = card;
        binder.readBean(this.card);
        setVisible(true);
    }

    private void save() {
        if (binder.isValid()) {
            try {
                binder.writeBean(card);
                cardService.createCard(card);
            } catch (Exception e) {
                Notification.show("Unable to save!");
            }
            exit();
        } else {
            Notification.show("Unable to save! Please check entered data card.");
        }
    }

    private void exit() {
        setVisible(false);
        mv.updateCardList();
    }

    private void prepareFields() {
        binder.forField(name)
                .withValidator(e -> Pattern.compile(namePattern).matcher(e).find(), "Name length min 3 symbols!")
                .asRequired("Pleas enter a name").bind(Card::getName, Card::setName);
        binder.forField(number)
                .withValidator(e -> Pattern.compile(numberPattern).matcher(e).find(), "Enter number at format: XXXX XXXX XXXX XXXX")
                .asRequired("Pleas enter a number card").bind(Card::getNumber, Card::setNumber);
        binder.forField(date)
                .withValidator(e -> Pattern.compile(datePattern).matcher(e).find(), "Enter date at format: XX/XX")
                .asRequired("Pleas enter date card").bind(Card::getDate, Card::setDate);
    }

}

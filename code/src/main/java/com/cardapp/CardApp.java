package com.cardapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
@PropertySource("classpath:config.properties")
public class CardApp {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        SpringApplication.run(CardApp.class, args);
    }
}

package com.cardapp.models;

import lombok.*;

import javax.persistence.*;

@Entity(name = "cards")
@NoArgsConstructor
@Getter
public class Card {

    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    @Setter
    private String name;

    @Column(length = 19, unique = true)
    @Setter
    private String number;

    @Column
    @Setter
    private String date;

    public Card(String name, String number, String date) {
        this.name = name;
        this.number = number;
        this.date = date;
    }

    @Override
    public String toString() {
        return name + ':' +
                "\nnumber: " + number +
                "\ndate: " + date;
    }
}

package com.cardapp.models;

import lombok.*;

import javax.persistence.*;

@Entity(name = "users")
@NoArgsConstructor
@Getter
@ToString
public class User {

    @Id
    @Column(unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    @Setter
    private String login;

    @Column
    @Setter
    private String password;

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

}

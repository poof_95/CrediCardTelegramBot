package com.cardapp.repository;

import com.cardapp.models.Card;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepo extends CrudRepository<Card,Long> {
}
